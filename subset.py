import sys
import numpy as np
import shapely.geometry
import geojson
import icesat
import datasets

def polygonize(collection):
    r"""Return a shapely polygon from a specially formatted GeoJSON feature
    collection"""
    shell = np.array(list(geojson.utils.coords(collection['features'][0])))
    holes = [np.array(list(geojson.utils.coords(feature)))
             for feature in collection['features'][1:]]
    return shapely.geometry.Polygon(shell, holes)


def read_intersecting(filenames, polygon):
    r"""Read the IceSat-2 data in filenames and return a dictionary of the data
    read if it intersects the given polygon"""
    output_data = {}
    for filename in filenames:
        data = icesat.read(filename)
        lines = [shapely.geometry.LineString(data[track]['coords'])
                 for track in ['gt1l', 'gt1r', 'gt2l', 'gt2r', 'gt3l', 'gt3r']]
        if any([polygon.intersects(line) for line in lines]):
            output_data[filename] = data

    return output_data


def inside_shape_bitmap(data, track, polygon):
    r"""Return a boolean array indicating whether the points on the given track
    are contained in a polygon"""
    coords = data[track]['coords']
    iterable = (polygon.contains(shapely.geometry.Point(pt)) for pt in coords)
    return np.fromiter(iterable, bool, coords.shape[0])


def high_quality_bitmap(data, track):
    r"""Return a boolean array indicating if the data point is good quality"""
    return data[track]['quality'] == 0


def get_segments(bitmap, threshold):
    r"""Return a list of pairs indicating the starting and finishing points of
    every contiguous segment of `True` values in the logical array

    https://stackoverflow.com/a/1066838"""
    bounds = np.hstack(([0], bitmap, [0]))
    delta = np.diff(bounds)
    run_start = np.where(delta > 0)[0]
    run_finish = np.where(delta < 0)[0]
    return (segment for segment in zip(run_start, run_finish)
            if segment[1] - segment[0] >= threshold)


if __name__ == '__main__':
    larsen_filename = datasets.fetch_larsen_outline()
    with open(larsen_filename, 'r') as geojson_file:
        larsen = geojson.load(geojson_file)
    polygon = polygonize(larsen)
    for filename, data in read_intersecting(sys.argv[1:], polygon).items():
        print(filename)
