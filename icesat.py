import numpy as np
import h5py
import pyproj

transformer = pyproj.Transformer.from_crs('epsg:4326', 'epsg:3031')

def read(filename):
    with h5py.File(filename, 'r') as f:
        result = {}
        for track in ['gt1l', 'gt1r', 'gt2l', 'gt2r', 'gt3l', 'gt3r']:
            lon = f[track]['land_ice_segments']['longitude'][:]
            lat = f[track]['land_ice_segments']['latitude'][:]
            x, y = transformer.transform(lat, lon)
            coords = np.stack((x, y)).T
            quality = f[track]['land_ice_segments']['atl06_quality_summary'][:]
            height = f[track]['land_ice_segments']['h_li'][:]
            result[track] = {
                'coords': coords,
                'height': height,
                'quality': quality
            }

    return result
