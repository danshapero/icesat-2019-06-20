import numpy as np
from scipy import interpolate
import geojson
import rasterio
import shapely.geometry
import subset
import datasets

larsen_filename = datasets.fetch_larsen_outline()
with open(larsen_filename, 'r') as geojson_file:
    larsen_geometry = geojson.load(geojson_file)
polygon = subset.polygonize(larsen_geometry)

with rasterio.open('larsen.tif', 'r') as dataset:
    profile = dataset.profile.copy()
    height, width = dataset.height, dataset.width
    bounds = dataset.bounds
    data = dataset.read(indexes=1, masked=True)

# Apparently there are nan values in the data that haven't been masked out?!
data.mask |= np.isnan(data)

# Also one spot is really goofed up
data.mask[130:135, 18:28] = True
data.mask[132:135, 9:14] = True

x = np.linspace(bounds.left, bounds.right, dataset.width)
y = np.linspace(bounds.top, bounds.bottom, dataset.height)

coords = []
surface = []
for i in range(height):
    for j in range(width):
        if not data.mask[i, j]:
            coords.append((x[j], y[i]))
            surface.append(data[i, j])

interpolator = interpolate.LinearNDInterpolator(coords, surface)
region = polygon.buffer(distance=5e3)
fdata = data.copy()

for i in range(height):
    for j in range(width):
        point = shapely.geometry.Point((x[j], y[i]))
        if data.mask[i, j] and region.contains(point):
            fdata[i, j] = interpolator((x[j], y[i]))

with rasterio.open('larsen-fix.tif', 'w', **profile) as output:
    output.write(fdata, indexes=1)
