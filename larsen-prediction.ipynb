{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import rasterio\n",
    "import firedrake\n",
    "import icepack, icepack.plot, icepack.models"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Input data\n",
    "\n",
    "We'll back out the thickness from the surface elevation, assuming that the ice is floating.\n",
    "For that, we also need a geoid height."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "surface = rasterio.open('larsen-fix.tif', 'r')\n",
    "geoid = rasterio.open('geoid.tif', 'r')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We're going to run our simulation on an unstructured triangular mesh."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mesh = firedrake.Mesh('larsen.msh')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "icepack.plot.triplot(mesh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we need to decide how to represent fields (thickness, velocity, etc.) on this domain.\n",
    "Here we'll use continuous Galerkin (CG) elements of polynomial degree 1 in each triangle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Q = firedrake.FunctionSpace(mesh, family='CG', degree=1)\n",
    "V = firedrake.VectorFunctionSpace(mesh, family='CG', degree=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `interpolate` function takes in a rasterio dataset and samples it at all of the interpolation points of the function space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = icepack.interpolate(surface, Q)\n",
    "g = icepack.interpolate(geoid, Q)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To calculate the thickness, we remove the geoid and firn air content from the surface elevation and divide by the buoyancy factor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from icepack.constants import ice_density as ρ_I, water_density as ρ_W\n",
    "firn_air = 1.8\n",
    "h = icepack.interpolate((s - g - firn_air) / (1 - ρ_I / ρ_W), Q)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I'll read in the rheology parameter $\\theta$ and the ice velocity $u$ from a simulation that I ran earlier.\n",
    "We can look at the inverse code too if you're curious!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "θ = firedrake.Function(Q)\n",
    "chk = firedrake.DumbCheckpoint('parameter', mode=firedrake.FILE_READ)\n",
    "chk.load(θ, name='parameter')\n",
    "\n",
    "u = firedrake.Function(V)\n",
    "chk = firedrake.DumbCheckpoint('velocity', mode=firedrake.FILE_READ)\n",
    "chk.load(u, name='velocity')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Modeling\n",
    "\n",
    "Now we need to create a model object.\n",
    "Here I'm showing an example of how you can customize components of the model physics, in this case the viscosity."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from icepack.constants import glen_flow_law as n\n",
    "A0 = icepack.rate_factor(260)\n",
    "def viscosity(u, h, θ):\n",
    "    A = A0 * firedrake.exp(-θ / n)\n",
    "    return icepack.models.viscosity.viscosity_depth_averaged(u, h, A)\n",
    "\n",
    "ice_shelf = icepack.models.IceShelf(viscosity=viscosity)\n",
    "opts = {'dirichlet_ids': [2, 4, 5, 6, 7, 8], 'tol': 1e-6}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u0 = u.copy(deepcopy=True)\n",
    "h0 = h.copy(deepcopy=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "δt = 1.0/12\n",
    "num_steps = 18\n",
    "T = δt * num_steps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running a prognostic model is a simple loop:\n",
    "* Update the the thickness by one timestep through mass conservation\n",
    "* Calculate the new value of the velocity through membrane stress balance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = firedrake.Constant(0)\n",
    "for step in range(num_steps + 1):\n",
    "    h = ice_shelf.prognostic_solve(δt, h0=h, u=u, a=a, h_inflow=h0)\n",
    "    u = ice_shelf.diagnostic_solve(u0=u, h=h, θ=θ, **opts)\n",
    "    \n",
    "    print('.' if step % 2 == 0 else '', flush=True, end='')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "contours = icepack.plot.tricontourf(h, 40, axes=axes)\n",
    "fig.colorbar(contours)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Look, the rifts!\n",
    "They're moving!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "δh = firedrake.interpolate(h - h0, Q)\n",
    "fig, axes = icepack.plot.subplots()\n",
    "contours = icepack.plot.tricontourf(δh, levels=np.linspace(-32, +32, 65),\n",
    "                                    extend='both', cmap='RdBu', axes=axes)\n",
    "fig.colorbar(contours)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compare with IceSat-2\n",
    "\n",
    "Finally we'll load in the IceSat-2 elevation, convert it to equivalent thickness, and compare."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import geojson\n",
    "import icesat\n",
    "import subset\n",
    "import datasets\n",
    "\n",
    "outline_filename = datasets.fetch_larsen_outline()\n",
    "with open(outline_filename, 'r') as outline_file:\n",
    "    larsen = geojson.load(outline_file)\n",
    "polygon = subset.polygonize(larsen)\n",
    "\n",
    "icesat2_filename = datasets.fetch_icesat2()\n",
    "icesat2_data = icesat.read(icesat2_filename)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "track = 'gt1r'\n",
    "bitmap = np.logical_and(subset.inside_shape_bitmap(icesat2_data, track, polygon),\n",
    "                        subset.high_quality_bitmap(icesat2_data, track))\n",
    "segments = list(subset.get_segments(bitmap, 1000))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "segment = segments[0]\n",
    "coords = icesat2_data[track]['coords'][segment[0]: segment[1]]\n",
    "height = icesat2_data[track]['height'][segment[0]: segment[1]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gd = np.array(list(geoid.sample(coords, indexes=1)))[:, 0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "icesat_thickness = (height - gd - firn_air) / (1 - ρ_I / ρ_W)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_thickness = np.zeros(len(icesat_thickness))\n",
    "for index in range(len(icesat_thickness)):\n",
    "    x = coords[index, :]\n",
    "    model_thickness[index] = h.at(x, tolerance=1e-6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "distance = np.hstack(([0], np.cumsum(np.sqrt(np.sum(np.diff(coords, axis=0)**2, axis=1)))))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots()\n",
    "axes.plot(distance / 1e3, icesat_thickness, label='ICESat-2')\n",
    "axes.plot(distance / 1e3, model_thickness, label='model')\n",
    "axes.set_xlabel('Distance (km)')\n",
    "axes.set_ylabel('Ice thickness')\n",
    "axes.legend()\n",
    "fig.savefig('model-data.png', dpi=300)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "error = icesat_thickness - model_thickness\n",
    "\n",
    "max_delta = np.max(np.abs(error))\n",
    "\n",
    "avg_delta = np.mean(error)\n",
    "std_delta = np.std(error - avg_delta)\n",
    "\n",
    "med_delta = np.median(error)\n",
    "mad_delta = np.median(np.abs(error - med_delta))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Error statistic   |')\n",
    "print('------------------|---------------------')\n",
    "print('max               |  {}'.format(max_delta))\n",
    "print('mean              |  {}'.format(avg_delta))\n",
    "print('sigma             |  {}'.format(std_delta))\n",
    "print('median            |  {}'.format(med_delta))\n",
    "print('median abs. dev.  |  {}'.format(mad_delta))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "firedrake",
   "language": "python",
   "name": "firedrake"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
