larsen.tif geoid.tif: dem.py
	python3 dem.py

larsen-fix.tif: larsen.tif interpolate_missing.py
	python3 interpolate_missing.py

larsen.msh parameter.h5 velocity.h5: larsen-fix.tif geoid.tif larsen-inverse.ipynb
	jupyter nbconvert larsen-inverse.ipynb --to ipynb --execute --ExecutePreprocessor.timeout=24000
