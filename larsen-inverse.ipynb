{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import rasterio\n",
    "import geojson\n",
    "import firedrake\n",
    "import icepack, icepack.plot, icepack.models\n",
    "import datasets\n",
    "import meshing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reading gridded data\n",
    "\n",
    "We can read in both geotiff and NetCDF using rasterio, the latter with a bit of extra magic to get the fields."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "surface = rasterio.open('larsen-fix.tif', 'r')\n",
    "geoid = rasterio.open('geoid.tif', 'r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "measures_filename = datasets.fetch_measures_antarctica()\n",
    "velocity_x = rasterio.open('netcdf:' + measures_filename + ':VX', 'r')\n",
    "velocity_y = rasterio.open('netcdf:' + measures_filename + ':VY', 'r')\n",
    "error_x = rasterio.open('netcdf:' + measures_filename + ':STDX', 'r')\n",
    "error_y = rasterio.open('netcdf:' + measures_filename + ':STDY', 'r')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Interpolating to the mesh\n",
    "\n",
    "First we'll read in a computational mesh and plot it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "outline_filename = datasets.fetch_larsen_outline()\n",
    "with open(outline_filename, 'r') as outline_file:\n",
    "    outline = geojson.load(outline_file)\n",
    "    \n",
    "geometry = meshing.collection_to_geo(outline)\n",
    "with open('larsen.geo', 'w') as geo_file:\n",
    "    geo_file.write(geometry.get_code())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmsh -2 -format msh2 -v 2 -o larsen.msh larsen.geo"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mesh = firedrake.Mesh('larsen.msh')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "icepack.plot.triplot(mesh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we create some function spaces using continuous Galerkin (CG) elements with polynomial degree 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Q = firedrake.FunctionSpace(mesh, family='CG', degree=1)\n",
    "V = firedrake.VectorFunctionSpace(mesh, family='CG', degree=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we interpolate all our observational data and the geoid to the computational mesh."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = icepack.interpolate(surface, Q)\n",
    "g = icepack.interpolate(geoid, Q)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u_obs = icepack.interpolate((velocity_x, velocity_y), V)\n",
    "σx = icepack.interpolate(error_x, Q)\n",
    "σy = icepack.interpolate(error_y, Q)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now to back out the shelf thickness from the surface elevation.\n",
    "We need to correct for the geoid and firn air content."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from icepack.constants import ice_density as ρ_I, water_density as ρ_W\n",
    "firn_air = 1.8\n",
    "h = icepack.interpolate((s - g - firn_air) / (1 - ρ_I / ρ_W), Q)\n",
    "print(np.min(h.dat.data_ro[:]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "contours = icepack.plot.tricontourf(h, 51, extend='both', axes=axes)\n",
    "fig.colorbar(contours)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Modeling\n",
    "\n",
    "Next we'll create an ice shelf model object.\n",
    "We'll modify how it computes the viscosity to make it easier to back out the rheology."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T0 = 260\n",
    "A0 = icepack.rate_factor(T0)\n",
    "from icepack.constants import glen_flow_law as n\n",
    "def viscosity(u, h, θ):\n",
    "    A = A0 * firedrake.exp(-θ / n)\n",
    "    return icepack.models.viscosity.viscosity_depth_averaged(u, h, A)\n",
    "\n",
    "θ = firedrake.Function(Q)\n",
    "\n",
    "ice_shelf = icepack.models.IceShelf(viscosity=viscosity)\n",
    "opts = {'dirichlet_ids': [2, 4, 5, 6, 7, 8], 'tol': 1e-6}\n",
    "u = ice_shelf.diagnostic_solve(u0=u_obs, h=h, θ=θ, **opts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "contours = icepack.plot.tricontourf(u, 20, axes=axes)\n",
    "fig.colorbar(contours)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Minimize the mean-square misfit with the observed velocities, but make the inferred field fairly smooth."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from firedrake import inner, grad, dx\n",
    "import icepack.inverse\n",
    "\n",
    "def objective(u):\n",
    "    return 0.5 * (((u[0] - u_obs[0]) / σx)**2 + ((u[1] - u_obs[1]) / σy)**2) * dx\n",
    "\n",
    "L = 4e3\n",
    "def regularization(θ):\n",
    "    return 0.5 * L**2 * inner(grad(θ), grad(θ)) * dx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define an inverse problem, a function to call at every iteration of the solver, and a solver object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "problem = icepack.inverse.InverseProblem(\n",
    "    model=ice_shelf,\n",
    "    method=icepack.models.IceShelf.diagnostic_solve,\n",
    "    objective=objective,\n",
    "    regularization=regularization,\n",
    "    state_name='u',\n",
    "    state=u,\n",
    "    parameter_name='θ',\n",
    "    parameter=θ,\n",
    "    model_args={'h': h, 'u0': u, 'tol': 1e-6},\n",
    "    dirichlet_ids=opts['dirichlet_ids']\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "area = firedrake.assemble(firedrake.Constant(1) * dx(mesh))\n",
    "def callback(solver):\n",
    "    E = firedrake.assemble(solver.objective)\n",
    "    R = firedrake.assemble(solver.regularization)\n",
    "    print('{:g}, {:g}'.format(E/area, R/area))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Solve the inverse problem and report the progress as we go."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solver = icepack.inverse.GaussNewtonSolver(problem, callback)\n",
    "iterations = solver.solve(\n",
    "    rtol=5e-3,\n",
    "    atol=0.0,\n",
    "    max_iterations=30\n",
    ")\n",
    "\n",
    "chk = firedrake.DumbCheckpoint('parameter', mode=firedrake.FILE_CREATE)\n",
    "chk.store(solver.parameter, name='parameter')\n",
    "\n",
    "chk = firedrake.DumbCheckpoint('velocity', mode=firedrake.FILE_CREATE)\n",
    "chk.store(solver.state, name='velocity')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the inferred parameter.\n",
    "Negative values are more fluid, positive values less fluid.\n",
    "See anything sensible or otherwise?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "θ = solver.parameter\n",
    "fig, axes = icepack.plot.subplots()\n",
    "contours = icepack.plot.tricontourf(θ, np.linspace(-10, 10, 41),\n",
    "                                    cmap='RdBu', extend='both', axes=axes)\n",
    "axes.set_xlabel('x (km)')\n",
    "axes.set_ylabel('y (km)')\n",
    "axes.set_title('Inferred fluidity parameter of Larsen C')\n",
    "fig.colorbar(contours)\n",
    "fig.savefig('parameter.png', dpi=300)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u = solver.state\n",
    "fig, axes = icepack.plot.subplots()\n",
    "streamlines = icepack.plot.streamplot(u, density=2500, precision=1000, axes=axes)\n",
    "fig.colorbar(streamlines)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "firedrake",
   "language": "python",
   "name": "firedrake"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
