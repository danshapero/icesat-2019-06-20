import os
from getpass import getpass
import subprocess
import requests
import pooch


def _earthdata_downloader(url, output_file, dataset):
    username = os.environ.get('EARTHDATA_USERNAME')
    if username is None:
        username = input('EarthData username: ')

    password = os.environ.get('EARTHDATA_PASSWORD')
    if password is None:
        password = getpass('EarthData password: ')

    login = requests.get(url)
    downloader = pooch.HTTPDownloader(auth=(username, password))
    downloader(login.url, output_file, dataset)


measures_antarctica = pooch.create(
    path=pooch.os_cache('icepack'),
    base_url='https://n5eil01u.ecs.nsidc.org/MEASURES/NSIDC-0484.002/1996.01.01/',
    registry={
        'antarctica_ice_velocity_450m_v2.nc':
        '268be94e3827b9b8137b4b81e3642310ca98a1b9eac48e47f91d53c1b51e4299'
    }
)

def fetch_measures_antarctica():
    return measures_antarctica.fetch('antarctica_ice_velocity_450m_v2.nc',
                                     downloader=_earthdata_downloader)


icesat2 = pooch.create(
    path = pooch.os_cache('icepack'),
    base_url='https://n5eil01u.ecs.nsidc.org/ATLAS/ATL06.001/2018.12.11/',
    registry={
        'ATL06_20181211172053_11310112_001_01.h5':
        '40e87dc1931220888c22b1760649d4eea7995038777c9709735063ce06abb0f4'
    }
)

def fetch_icesat2():
    return icesat2.fetch('ATL06_20181211172053_11310112_001_01.h5',
                         downloader=_earthdata_downloader)


larsen_outline = pooch.create(
    path=pooch.os_cache('icepack'),
    base_url='https://raw.githubusercontent.com/icepack/glacier-meshes/master/glaciers/',
    registry={
        'larsen.geojson':
        '74a632fcb7832df1c2f2d8c04302cfcdb3c1e86e027b8de5ba10e98d14d94856'
    }
)

def fetch_larsen_outline():
    return larsen_outline.fetch('larsen.geojson')


rema_1km = pooch.create(
    path=pooch.os_cache('icepack'),
    base_url='http://data.pgc.umn.edu/elev/dem/setsm/REMA/mosaic/v1.1/1km/',
    registry={
        'REMA_1km_dem.tif':
        'e6544528803375d3034e4621e628d8934469d6bf9607869643b2abc7d8ec727a'
    }
)

def fetch_rema_1km():
    return rema_1km.fetch('REMA_1km_dem.tif')


egm2008 = pooch.create(
    path=pooch.os_cache('icepack'),
    base_url='https://sourceforge.net/projects/geographiclib/files/geoids-distrib/',
    registry={
        'egm2008-5.tar.bz2':
        '9a57c14330ac609132d324906822a9da9de265ad9b9087779793eb7080852970'
    }
)

def fetch_egm2008():
    filenames = egm2008.fetch('egm2008-5.tar.bz2', processor=pooch.Untar())
    return [f for f in filenames if os.path.splitext(f)[1] == '.pgm'][0]
